package ec.edu.decc.numeros;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private Button btCheck;
    private EditText etInputNumber;
    private String input_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.btCheck = (Button) findViewById(R.id.bt_check);
        setButtonCheckEvent();

        this.etInputNumber = (EditText) findViewById(R.id.base_number);
    }

    private void setButtonCheckEvent()  {
        this.btCheck.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                input_number = etInputNumber.getText().toString();
                int intNumber;
                try{
                    intNumber = Integer.parseInt(input_number);
                }catch (Exception e){
                    intNumber = 0;
                }
                if (intNumber > 0) {
                    Intent intent = new Intent(MainActivity.this, CalculationActivity.class);
                    intent.putExtra("input_number", input_number);
                    startActivity(intent);
                }
            }
        });
    }
}
