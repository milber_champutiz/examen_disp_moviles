package ec.edu.decc.numeros;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class CalculationActivity extends AppCompatActivity {

    private TextView twInputNumber;
    private CheckBox cbPrimo;
    private CheckBox cbFeliz;
    private CheckBox cbDeficiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculation);

        Intent intent = getIntent();
        String input_number = intent.getStringExtra("input_number");
        int intNumber = Integer.parseInt(input_number);

        twInputNumber = (TextView) findViewById(R.id.input_number);
        twInputNumber.setText(twInputNumber.getText().toString() + ' ' + intNumber);

        Button backButton = (Button)this.findViewById(R.id.bt_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        cbPrimo = (CheckBox) findViewById(R.id.cbPrimo);
        cbFeliz = (CheckBox) findViewById(R.id.cbFeliz);
        cbDeficiente = (CheckBox) findViewById(R.id.cbDeficiente);

        checkConditions(intNumber);
    }

    private void checkConditions(int my_number){
        cbPrimo.setChecked(isPrime(my_number));
        cbFeliz.setChecked(isHappy(my_number));
        cbDeficiente.setChecked(isDeficient(my_number));
    }

    private boolean isPrime(int my_number){
        int count = 2;
        boolean prime = true;
        if (my_number > 3){
            while ((prime) && (count != my_number)){
                if (my_number % count == 0)
                    prime = false;
                count++;
            }
        }
        return prime;
    }

    private boolean isHappy(int my_number){
        boolean happy = false;
        int[] numbers = numberArray(my_number);
        int sum = squareSum(numbers);
        int numbersLength = numbers.length;
        while (numbersLength > 1 ){
            numbers = numberArray(sum);
            sum =  squareSum(numbers);
            numbersLength = numbers.length;
        }
        if (sum == 1)
            happy = true;
        return happy;
    }

    private boolean isDeficient(int my_number){
        boolean deficient = false;
        int sum = 0;
        for(int i=1; i<my_number; i++){
            if (my_number % i == 0){
                sum += i;
            }
        }
        if (sum < my_number)
            deficient = true;

        return deficient;
    }

    private static int[] numberArray(int my_number){
        String x = Integer.toString(my_number);
        int[] numbers = new int[x.length()];

        int iDigit = 0;
        while (my_number>0){
            numbers[iDigit] = my_number%10;
            my_number = my_number/10;
            iDigit++;
        }
        return numbers;
    }

    private int squareSum(int[] number_array){
        int sum = 0;
        for (int i=0; i<number_array.length; i++)
            sum += Math.pow(number_array[i],2);
        return sum;
    }
}
